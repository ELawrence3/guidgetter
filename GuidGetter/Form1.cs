﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace GuidGetter
{
    public partial class GUIGUIDGETTER : Form
    {
        public static bool debugging = false;
        List<entity> entities = new List<entity>();
        public String CSVHeader = "";
        public GUIGUIDGETTER()
        {
            InitializeComponent();
        }
        public struct entity
        {
            public string LDM_GUID;
            public string LDM_GUID_ENTITY;
            public string LDM_GUID_PRIMARY_KEY;
            public string LDM_GUID_LDM_NAME;

            public entity(string guid, string ent, string pkey, string name)
            {
                LDM_GUID = guid;
                LDM_GUID_ENTITY = ent;
                LDM_GUID_PRIMARY_KEY = pkey;
                LDM_GUID_LDM_NAME = name;
            }
        }

        public struct Environment
        {
            public string NAME;
            public string SERVER;
            public string DIRECTORY;
            public string API_URL;
            public string API_PORT;
            public Environment(string name)
            {
                NAME = name;
                DIRECTORY = "/datatel/coll18/" + name + "/apphome"; ;                
                
                //Only supports TEST so far
                SERVER = "";                
                API_PORT = "";
                API_URL = "";


                if (name.Contains("test"))
                {
                    SERVER = "coll-test.snhu.edu";
                    API_PORT = "810" + name[name.Length - 1];
                    API_URL = "https://sissvc-test.snhu.edu:" + API_PORT + "/ColleagueApi/";
                }
                else if(name.Contains("dev"))
                {
                    SERVER = "coll-dev.snhu.edu";
                    API_PORT = "8183";
                    API_URL = "https://sissvc-dev.snhu.edu:" + API_PORT + "/ColleagueApi/";

                }
                else if (name.Contains("stage"))
                {
                    SERVER = "coll-report.snhu.edu";
                    API_PORT = "8183";
                    API_URL = "https://sissvc-stage.snhu.edu:" + API_PORT + "/ColleagueApi/";

                }
            }
        }
        private void GET_Click(object sender, EventArgs e)
        {
            ControlsEnabled(false);
            string env = cb_env.Items[cb_env.SelectedIndex].ToString();
            switch (tab_SearchMethod.SelectedIndex)
            {
                case 0:
                    string ldmName = cb_ldmName.Items[cb_ldmName.SelectedIndex].ToString();
                    Task.Run(() => getGuidsByResource(ldmName, env));
                    break;
                case 1:
                    Task.Run(() => getGuidsFromCID(env));
                    break;
                case 2:
                    softException("this doesn't do anything");
                    break;
            }
            
        }

        private void softException(string exception)
        {
            Print(exception, false);
            ControlsEnabled(true);

        }

        private void ControlsEnabled(bool enable)
        {
            GET.Invoke(new Action(() => GET.Enabled = enable));
            OpenCSV.Invoke(new Action(() => OpenCSV.Enabled = enable));
        }

        private void getGuidsByResource(string ldmName, string env)
        {
            string filePath = tb_FileName.Text;
            string username = tb_username.Text;
            string pw = tb_password.Text;
            string sampleSize = "";            

            if(!chk_xml.Checked && !chk_csv.Checked && !chk_sql.Checked)
            {
                softException("Please chose at least one output file type.");
                return;
            }

            List<string> result = new List<string>();
            entities = new List<entity>();

            Environment environment = new Environment(env);
           

            if (!chk_GetAll.Checked)
            {
                if (num_sampleSize.Value < 1)
                {
                    softException("Please select a valid sample size.");
                    return;
                }
                else
                    sampleSize = " Sample " + num_sampleSize.Value;
            }

            //number of results returned from query
            int numResults = 0;
            
            try
            {
                string tempFile = "";
                //Verify output file is accessible before processing request
                Print("Verifying output file...", false);
                if (filePath.Length < 5)
                {
                    softException("Please select a valid filename");
                    return;
                }          
                //WriteToFile(filePath, CSVHeader, "");

                Print("Connecting to " + environment.SERVER + " via SSH...", false);
                using (var client = new SshClient(environment.SERVER, username, pw))
                {   
                    client.Connect();//sign in to ssh             
                    
                    Print("Creating shell stream...", false);                    
                    using (ShellStream shell = client.CreateShellStream("shell", 0, 0, 0, 0, 1024))
                    {
                        //Launch UDT and enter password
                        PasswordPrompt(environment.DIRECTORY, pw, shell);
                        Print("Fetching " + ldmName + "...", false);
                        string SFTPFileName = "";
                        if (ldmName == "CIMC")
                        {
                            SFTPFileName = "EWL.GUID.GETTER" + RandomString(5);
                            SendCommand("LIST CDM.INTEGRATION BY.EXP CINT.API.RESOURCE WITH @ID EQ 'HUB' CINT.API.RESOURCE CINT.API.RSRC.SCHEMA.VER CINT.API.RSRC.SCHEMA.SEM.VER CINT.API.PATH CINT.API.BUS.EVENTS ID.SUP TO DELIM ',' _HOLD_/" + SFTPFileName, shell, 500);
                        }
                        else if(ldmName == "EAEE")
                        {
                            SFTPFileName = "EWL.GUID.GETTER" + RandomString(5);
                            SendCommand("SORT EDM.EXT.VERSIONS BY EDMV.RESOURCE.NAME EDMV.RESOURCE.NAME EDMV.VERSION.NUMBER EDMV.COLUMN.NAME EDMV.INQUIRY.FIELDS ID.SUP TO DELIM ',' _HOLD_/" + SFTPFileName, shell, 500);
                        }
                        else if (ldmName == "EDTC")
                        {
                            SFTPFileName = "EWL.GUID.GETTER" + RandomString(5);
                            SendCommand("LIST TRIGGER.CONDITIONS BY @ID TRIGCON.SEL.FIELD TRIGCON.OPERATOR TRIGCON.SEL.VALUE TRIGCON.COMMENTS WITH @ID LIKE 'INTG...' TO DELIM ' | ' _HOLD_/" + SFTPFileName, shell, 500);
                        }
                        else
                        {
                            SFTPFileName = UDT_SAVELIST(shell, "SELECT LDM.GUID WITH LDM.GUID.LDM.NAME LIKE '..." + ldmName + "...'" + sampleSize);
                        }

                        
                        if (SFTPFileName == "")
                        {
                            softException("No data retrieved from current (S)SELECT statement.");
                            return;
                        }
                         tempFile = DownloadFile(environment, username, pw, SFTPFileName);




                        //result = GetUDTReponse(shell, "LIST LDM.GUID WITH LDM.GUID.LDM.NAME EQ '" + ldmName + "' LDM.GUID.ENTITY LDM.GUID.PRIMARY.KEY LDM.GUID.LDM.NAME" + sampleSize);

                        Print("Closing Session...",false);
                        SendCommand("quit", shell, 500);
                        SendCommand("exit", shell, 500);
                        shell.Close();
                        //shell.Dispose();                        
                    }
                    client.Disconnect();
                    client.Dispose();
                }

                Print("Processing Data...", false);
                string headers = "";
                char delim = ',';
                List<string> headerList = new List<string>();                
                if (ldmName == "CIMC" || ldmName == "EAEE" || ldmName == "EDTC")
                {
                    if (ldmName == "CIMC")
                        headerList = new List<string>() { "CINT.API.RESOURCE", "CINT.API.RSRC.SCHEMA.VER", "CINT.API.PATH", "CINT.API.BUS.EVENTS" };
                    else if (ldmName == "EAEE")
                        headerList = new List<string>() { "EDMV.RESOURCE.NAME", "EDMV.VERSION.NUMBER","EDMV.COLUMN.NAME", "EDMV.INQUIRY.FIELDS" };
                    else if (ldmName == "EDTC")
                    {
                        headerList = new List<string>() { "TRIGGER.CONDITIONS", "TRIGCON.SEL.FIELD", "TRIGCON.OPERATOR", "TRIGCON.SEL.VALUE", "TRIGCON.COMMENTS" };
                        delim = ' ';
                    }
                    numResults = ProcessCIMC(ldmName, tempFile, filePath, headerList, delim);                          
                    File.Delete(tempFile);
                }
                else
                {
                    headers = "ENV,LDM.GUID,LDM.GUID.ENTITY,LDM.GUID.PRIMARY.KEY,LDM.GUID.LDM.NAME";
                    string guid = "", ent = "", pkey = "", name = "";
                    using (System.IO.StreamReader file =
                        new System.IO.StreamReader(tempFile))
                    {
                        string line = "";
                        StringBuilder body = new StringBuilder();
                        while ((line = file.ReadLine()) != null)
                        {
                            numResults++;
                            result = line.Split(',').ToList<string>();

                            for(int i = 0; i < result.Count; i++)
                            {
                                switch (i % 4)
                                {
                                    case 0:
                                        guid = result[i];
                                        break;
                                    case 1:
                                        ent = result[i];
                                        break;
                                    case 2:
                                        pkey = result[i];
                                        break;
                                    case 3:
                                        name = result[i];
                                        entities.Add(new entity(guid, ent, pkey, name));
                                        break;
                                    default:
                                        Print("Error Line 150: Incorrect line count", true);
                                        break;
                                }
                            }
                        }                    
                    }                
                    File.Delete(tempFile);

                    if (chk_csv.Checked)
                    {
                        string body = CreateGUID_CSV(filePath, chk_append.Checked, headers);
                        //write all objects to spreadsheet
                        //WriteToFile(filePath + ".csv", CSVHeader, body, true);
                    }
                    if(chk_xml.Checked)
                    {
                        CreateGUID_XML(filePath);
                    }                    
                    if (chk_sql.Checked)
                    {
                        CreateGUID_SQL(filePath);
                    }
                }
                Print("Job complete! " + numResults + " Record(s) saved to " + filePath, false);

                //UNDER DEVELOPMENT:  The following functionality is under development and should not be used in a production environment.
                /*string token = Authenticate(environment.API_URL);
                foreach (entity en in entities)//for debugging
                {
                    Console.WriteLine(GEThttp(environment, en.LDM_GUID_LDM_NAME, en.LDM_GUID, token));
                }*/
                entities.Clear();
                
            }
             catch(System.Net.WebException) { }
             catch (Exception ex)
             {
                 if(debugging)
                     Print(ex.Message + "\r\n" + ex.StackTrace, false);
                 else
                     Print(ex.Message,false);
             }
            ControlsEnabled(true);
        }
      public int ProcessCIMC(string ldmname, string tempFile, string filePath, List<string> headers, char delim)
        {
            StreamReader sr = new StreamReader(tempFile);
            int columnCount = sr.ReadLine().Split(delim).Length;
            int rowCounter = 0;
            DataTable dt = new DataTable();
            for(int i=0;i<columnCount;i++)// string header in headers)
            {
                dt.Columns.Add("Column_"+i);
            }
            sr.Close();
            sr = new StreamReader(tempFile);
            while (!sr.EndOfStream)
            {
                string[] rows = sr.ReadLine().Split(delim);
                DataRow dr = dt.NewRow();
                for (int i = 0; i < columnCount; i++)
                {
                    if (delim != ',')
                        dr[i] = rows[i].Replace(',', ';');
                    else
                        dr[i] = rows[i];
                }
                dt.Rows.Add(dr);
                rowCounter++;
            }

            DataTable dt2 = new DataTable();
            foreach (string header in headers)
                dt2.Columns.Add(header);

            foreach (DataRow row in dt.Rows)
            {
                DataRow dr2 = dt2.NewRow();
                int j = 0;
                for (int i = 0; i < dt2.Columns.Count; i++)
                {
                    if (i == 1 && ldmname == "CIMC")
                    {
                        dr2[i] = mergeVersionColumns(row[i].ToString(), row[i + 1].ToString());
                        j = 1;
                    }
                    else
                        dr2[i] = row[i + j];
                }

                dt2.Rows.Add(dr2);
            }

            using (System.IO.StreamWriter file =
            new StreamWriter(File.Open(filePath+".csv", FileMode.Create), Encoding.UTF8))
            {

                for (int i = 0; i < dt2.Columns.Count; i++)
                {
                    if (i + 1 == dt2.Columns.Count)
                        file.WriteLine(dt2.Columns[i].ColumnName);
                    else
                        file.Write(dt2.Columns[i].ColumnName + ",");
                }

                foreach (DataRow datarow in dt2.Rows)
                {
                    for (int i = 0; i < datarow.ItemArray.Length; i++)// var item in datarow.ItemArray)
                    {
                        if (i + 1 == datarow.ItemArray.Length)
                            file.WriteLine(datarow.ItemArray[i]);
                        else
                            file.Write(datarow.ItemArray[i] + ",");
                    }
                }
            }
            sr.Close();
            return rowCounter;
        }

        public static string mergeVersionColumns(string A, string B)
        {

            /*  if (A == B) // responsible for merging columns
                  return A;
              else if (A == "")
                  return B;
              else if (B == "")
                  return A;
              else if (Convert.ToInt32(A) > Convert.ToInt32(B))
                  return A;
              else*/
            return B;
        }
        public List<string> CleanList(List<string> response, ShellStream shell)
        {
            if(response[response.Count-1].Contains("<New line>"))
            {
                List<string> reply = SendCommand("", shell, 25).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>();
                response.RemoveAt(response.Count - 1);
                response.AddRange(CleanList(reply, shell));
            }
            for (int i = 0; i < response.Count; i++)
            {
                while ((i < response.Count) && (response[i].Length <= 3 || response[i].Contains("<New line>") || response[i].Contains(":")))
                {
                    response.RemoveAt(i);
                }
            }

            return response;
        }

        public string UDT_SAVELIST(ShellStream shell, string command)
        {
            int maxRetries = 600;
            int delay = 1000;
            Print("Creating Selection...", false);
            List<string> result = SendCommand(command, shell, delay).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>();
            
            int sentinel = 1;
            while (result[result.Count - 1] != ">" && sentinel <= maxRetries)
            {                
                foreach(string s in SendCommand("", shell, delay).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>())
                {                    
                    if (s==">")
                    {
                        result.Add(s);
                        break;
                    }
                    else if (s.Contains("No data"))
                        return "";
                }
                //result.AddRange(SendCommand("", shell, delay).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>());
                
            }

            Print("Creating SAVELIST...", false);
            string randomString = RandomString(5);
            result = SendCommand("SAVE.LIST EWL.GUID.GETTER" + randomString , shell, 2000).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>();            
            sentinel = 1;
            while (!result[result.Count - 1].Contains(">") && result[result.Count - 1].Length != 1 && sentinel <= maxRetries)
            {
                result.AddRange(SendCommand("", shell, delay).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>());

                if (result[result.Count - 1] == "No data retrieved from current (S)SELECT statement.")
                    return "";
            }            


            Print("Getting SAVELIST...", false);
            result = SendCommand("GETLIST EWL.GUID.GETTER" + randomString, shell, 2000).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>();
            sentinel = 1;
            while (!result[result.Count - 1].Contains(">") && result[result.Count - 1].Length != 1 && sentinel <= maxRetries)
            {
                result.AddRange(SendCommand("", shell, delay).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>());
            }

            Print("Saving File to _HOLD_...", false);

            string filename = RandomString(5);
            result = SendCommand("LIST LDM.GUID BY LDM.GUID.PRIMARY.KEY LDM.GUID.ENTITY LDM.GUID.PRIMARY.KEY LDM.GUID.LDM.NAME TO DELIM \", \" _HOLD_/EWL" + filename + ".TXT", shell, 2000).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>();
            sentinel = 1;
            while (!result[result.Count - 1].Contains(">") && result[result.Count - 1].Length != 1 && sentinel <= maxRetries)
            {
                result.AddRange(SendCommand("", shell, delay).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>());
            }
            
            return "EWL"+filename+".TXT";
        }
        public string DownloadFile(Environment environment, string username, string password, string filename)
        {
            string host = environment.SERVER;
            string localFileName = System.IO.Path.GetFileName("temp.csv");
            string remoteFileName = environment.DIRECTORY + "/_HOLD_/" + filename;
            Print("Connecting to SFTP Server...", false);
            
            using (var sftp = new SftpClient(host, username, password))
            {
                sftp.Connect();               


                Print("Downloading SAVELIST File...", false);
                using (var file = File.OpenWrite(localFileName))
                {
                    sftp.DownloadFile(remoteFileName, file);
                    sftp.DeleteFile(remoteFileName);
                }

                sftp.Disconnect();
            }

            return localFileName;
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public List<string> GetUDTReponse(ShellStream shell, string command)
        {
            int maxRetries = 2000;
            int delay = 2000;
            List<string> result = CleanList(SendCommand(command, shell, delay).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>(), shell);


            int sentinel = 1;
            //Some queries take up to 7 minutes to parse entire flat file. This loop will allow up to 10 to ensure all results are recieved.
            //The while loop looks for ':' which indicates the console is ready for user input and has therefore completed its task.
            //The streamreader will update every 10 seconds up to a maximum of 60 times.
            while (!result[result.Count - 1].Contains("listed") && sentinel <= maxRetries)
            {
                delay = 250;
                Print("Warning: This query may take several minutes to complete" + System.Environment.NewLine + "Refreshing stale Session..." + sentinel / 20 + @"/" + maxRetries / 20, false);
                result.AddRange(CleanList(SendCommand("", shell, delay).Split(new[] { System.Environment.NewLine }, StringSplitOptions.None).ToList<string>(), shell));
                sentinel++;
            }

            if (debugging)
            {
                StringBuilder body = new StringBuilder();
                foreach (string line in result)
                {
                    body.Append(line + System.Environment.NewLine);
                }
                WriteToFile("test.csv", "", body.ToString(), false);
            }
            return result;
        }
        public string CreateGUID_CSV(string filePath, bool append, string headers)
        {
            string fileName = filePath + ".csv";
            CSVHeader = headers;
            string env = "";
            cb_env.Invoke(new Action(() => env = cb_env.Items[cb_env.SelectedIndex].ToString()));
            bool newFile = false;
            if (!File.Exists(fileName))
            {
                new FileInfo(fileName).Directory.Create();
                newFile = true;
            }
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(fileName, append))
            {
                if (newFile || !append)
                {
                    Console.WriteLine("Printing Header");
                    file.WriteLine(CSVHeader);
                }
                         


                //StringBuilder body = new StringBuilder();
                foreach (entity en in entities)
                {
                    //cb_env.Invoke(new Action(() => body.Append(cb_env.Items[cb_env.SelectedIndex].ToString() + "," + en.LDM_GUID + "," + en.LDM_GUID_ENTITY + "," + en.LDM_GUID_PRIMARY_KEY + "," + en.LDM_GUID_LDM_NAME + System.Environment.NewLine)));
                    file.WriteLine(env + "," + en.LDM_GUID + "," + en.LDM_GUID_ENTITY + "," + en.LDM_GUID_PRIMARY_KEY + "," + en.LDM_GUID_LDM_NAME);
                }
            }
            return "404";//body.ToString();
        }

        public string CreateGUID_XML(string filePath)
        {
            string fileName = filePath + ".xml";

            //StringBuilder body = new StringBuilder();    
            //body.Append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><resources name=\"" + entities[0].LDM_GUID_LDM_NAME + "\">");
            if (!File.Exists(fileName))
            {
                new FileInfo(fileName).Directory.Create();
            }
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(fileName, false))
            {



                file.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><resources name=\"" + entities[0].LDM_GUID_LDM_NAME + "\">");

//                int dumps = 0;
                //foreach (entity en in entities)
                for (int i = 0; i < entities.Count; i++)
                {
                    /*body.Append("<resource>" + System.Environment.NewLine + //too pretty
                        "<uri-params>" + System.Environment.NewLine + 
                        "<uri-param name=\"resourceKey\" value=\"" + entities[i].LDM_GUID + "\"/>" + System.Environment.NewLine +
                        "</uri-params>" + System.Environment.NewLine +
                        "<query-params/>" + System.Environment.NewLine +
                        "</resource>" + System.Environment.NewLine);*/


                    file.WriteLine("<resource><uri-params><uri-param name=\"resourceKey\" value=\"" + entities[i].LDM_GUID + "\"/></uri-params><query-params/></resource>");

                    /* body.Append("<resource><uri-params><uri-param name=\"resourceKey\" value=\"" + entities[i].LDM_GUID + "\"/></uri-params><query-params/></resource>");
                     if (body.Length >= 52428800 * (dumps+1))
                     {
                         WriteToFile(fileName, "", body.ToString(), dumps != 0);
                         body.Clear();
                         dumps++;
                     }*/
                }
                //body.Append("</resources>");

                file.WriteLine("</resources>");
            }
           // WriteToFile(fileName, "", body.ToString(), dumps != 0);
            return "404";
        }

        public string CreateGUID_SQL(string filePath)
        {
            string fileName = filePath + "-universe.sql";

            if (!File.Exists(fileName))
            {
                new FileInfo(fileName).Directory.Create();
            }
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(fileName, false))
            {

                file.WriteLine(@"use [IntegrationEvents]"+ System.Environment.NewLine +
                                "declare @user nvarchar(max) = 'SVC_SNHU_MULESOFT@snhudev.database.windows.net'" + System.Environment.NewLine +
                                "declare @now datetime2 = getdate()" + System.Environment.NewLine +
                                "declare @universe nvarchar(max) = ''" + System.Environment.NewLine +
                                "declare @TypeID bigint = 0" + System.Environment.NewLine +
                                "declare @QueryID bigint = 0");
                
                //file.Write("set @universe = @universe + '");    
                file.Write("set @universe = CONCAT('<?xml version=\"1.0\" encoding=\"UTF-8\" ?><resources name=\"" + entities[0].LDM_GUID_LDM_NAME + "\">','");
                // file.Write("set @universe = @universe + '");
                int j = 0;
                for (int i = 0; i < entities.Count; i++)
                {
                    
                    if ((i + 1) % 238 == 0)
                    {
                        //file.Write("'" + System.Environment.NewLine + "set @universe = @universe + '");
                        file.Write("','");
                        j++;
                    }
                    file.Write("<resource><uri-params><uri-param name=\"resourceKey\" value=\"" + entities[i].LDM_GUID + "\"/></uri-params><query-params/></resource>");
                    
                    if (j == 250)
                    {
                        file.Write("')" + System.Environment.NewLine + "set @universe = CONCAT(@universe,'");
                        j = 0;
                    }
                }
                //file.WriteLine("'");
                
                //file.WriteLine("set @universe = @universe + '</resources>'" + System.Environment.NewLine);
                file.WriteLine("</resources>')" + System.Environment.NewLine);

                file.WriteLine(@"insert into [IntegrationEvents].[dbo].[NotificationsUniverses]" + System.Environment.NewLine +
                                "(TypeID, QueryID, UniverseRunDate,Universe,CreatedBy,CreatedOn,ModifiedBy, ModifiedOn)" + System.Environment.NewLine +
                                "values" + System.Environment.NewLine +
                                "(@TypeID,@QueryID,@now,"+ /*test.ToString()*/ "@universe" + ",@user,@now,@user,@now)" + System.Environment.NewLine + System.Environment.NewLine +
                                "go");              
            }
            return "404";
        }
        private void getGuidsFromCID(string env)
        {
            if (!chk_xml.Checked && !chk_csv.Checked)
            {
                softException("Please chose at least one output file type.");
                return;
            }

            entities.Clear();
            Environment environment = new Environment(env);

            string resource = "";
            cb_resourceByID.Invoke(new Action(() => resource = cb_resourceByID.Items[cb_resourceByID.SelectedIndex].ToString()));

            //Validate Credentials
            try
            {
                string username = tb_username.Text;
                string pw = tb_password.Text;
                Print("Authenticating to " + environment.SERVER + " via SSH...", false);
                using (var client = new SshClient(environment.SERVER, username, pw)) {
                    client.Connect();//sign in to ssh 
                    Print("Success!  Retrieving data...", false);
                    client.Disconnect();
                    client.Dispose();
                }
            }catch(Exception ex)
            {
                softException("Line 274: " + ex.Message);
                return;
            }

            if (File.Exists(tb_csv_input.Text))
            {
                Console.WriteLine("Something went wrong");
                string token = Authenticate(environment.API_URL);
                string line = "";

                using (System.IO.StreamReader file =
                    new System.IO.StreamReader(tb_csv_input.Text))
                {
                    line = file.ReadLine();
                    StringBuilder body = new StringBuilder();
                    while ((line = file.ReadLine()) != null)
                    {
                        string jsonBody = GEThttp(environment, "persons", "?criteria={\"credentials\":[{\"type\":\"colleaguePersonId\",\"value\":\"" + line + "\"}]}", token);
                        string json = jsonBody.Substring(1, jsonBody.Length - 2);
                        Person person = JsonConvert.DeserializeObject<Person>(json);

                        if (chk_csv.Checked)
                            body.Append(JSONtoCSV(json) + System.Environment.NewLine);
                        if (chk_xml.Checked)
                            entities.Add(new entity(person.id, "PERSON", "", resource));                            
                    }

                    if(chk_csv.Checked)
                        WriteToFile(tb_FileName.Text + ".csv", CSVHeader, body.ToString(), true);
                    if (chk_xml.Checked)
                    {
                        string xml = CreateGUID_XML(tb_FileName.Text + ".xml");
                        WriteToFile(tb_FileName.Text + ".xml", CSVHeader, xml, false);
                    }                    
                }
                Print("Done!", false);
                ControlsEnabled(true);
            }
            else if(tb_CID.Text!="")
            {

                string token = Authenticate(environment.API_URL);
                entities = new List<entity>();

                string jsonBody = GEThttp(environment, "persons", "?criteria={\"credentials\":[{\"type\":\"colleaguePersonId\",\"value\":\"" + tb_CID.Text + "\"}]}", token);
                string json = jsonBody.Substring(1, jsonBody.Length - 2);
                Person person = JsonConvert.DeserializeObject<Person>(json);


                if (chk_csv.Checked)
                {
                    string CSVbody = JSONtoCSV(json);
                    WriteToFile(tb_FileName.Text + ".csv", CSVHeader, CSVbody, true);
                }
                if (chk_xml.Checked) { 
                    entities.Add(new entity(person.id, "PERSON", "", resource));
                    string xml = CreateGUID_XML(tb_FileName.Text + ".xml");
                    WriteToFile(tb_FileName.Text + ".xml", CSVHeader, xml, false);
                }

                dynamic Obj = JSONtoCSV(json);

                Print("Done!", false);
                ControlsEnabled(true);

            }
                
        }       


        public string JSONtoCSV(string json)
        {

            Person person = JsonConvert.DeserializeObject<Person>(json);

            Print("Processing Data...", false);

            List<KeyValuePair<string,string>> propertyInfo = new List<KeyValuePair<string,string>>();

            propertyInfo.Add(new KeyValuePair<string,string>("GUID",person.id));
            foreach (var credential in person.credentials)
            {
                if(credential.type=="colleaguePersonId")
                  propertyInfo.Add(new KeyValuePair<string, string>("ColleagueID", credential.value));
            }
            foreach (var name in person.names)
                if(name.type.category=="legal")
                    propertyInfo.Add(new KeyValuePair<string, string>("FullName_" + name.type.category, name.fullName));

            string csv = createCSV(",", propertyInfo, person); //ToCSV(",", enumerable);
            return csv;
        }
        public string createCSV(string separator, List<KeyValuePair<string,string>> piArr, object obj)
        {
            string csv = "";
            string header = "";

            for (int i = 0; i < piArr.Count; i++)
            {
                string punctuation = "";
                if (i < piArr.Count - 1)
                    punctuation = ",";

                header += piArr[i].Key + punctuation;
                csv += piArr[i].Value + punctuation;
            }
            Console.Write(header);
            if(CSVHeader=="")
                CSVHeader = header;

            return csv;
        }     

        private void WriteToFile(string filePath, string header, string body, bool append)
        {
            bool newFile = false;
            if (!File.Exists(filePath)) {
                new FileInfo(filePath).Directory.Create();
                newFile = true;
            }
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(filePath, append))
            {
                //Check if file exists. If not, add header row first.
                if ((newFile || !append) && header.Length>0)
                    file.WriteLine(header);
                if(body!="")
                    file.WriteLine(body);
            }
        }
        private void Print(string text, bool append)
        {
            if(append)
                output.Invoke(new Action(() => output.Text += text + System.Environment.NewLine));
            else
                output.Invoke(new Action(() => output.Text = text + System.Environment.NewLine));
        }

        private static void PasswordPrompt(string directory, string password, ShellStream stream)
        {
          
            string prompt = stream.Expect(new Regex(@"[$>]"));

            if(debugging)
                Console.WriteLine("Prompt 1: " + prompt);//debugging

            stream.WriteLine("cd " + directory + " && udt");
            Thread.Sleep(3000);
            

            prompt = stream.Expect(new Regex(@"([$#>:])"));
            if (prompt.Contains("Terminal shell is unknown"))
            {
                stream.WriteLine("export TERM=vt100");
                stream.WriteLine("export TERMCAP=$INFORMIXDIR/etc/termcap");
                stream.WriteLine("cd " + directory + " && udt");
                Thread.Sleep(3000);
                prompt = stream.Expect(new Regex(@"([$#>:])"));
            }

            if (debugging)
                Console.WriteLine("Prompt 2: " + prompt);//debugging

            if (prompt.Contains("Password"))
            {                
                stream.WriteLine(password);
                prompt = stream.Expect(new Regex(@"([$#>:])"));
                if (debugging)
                {
                    Console.WriteLine("PASSWORD PROMPT LOCATED!!!!!!");//debugging
                    Console.WriteLine("Prompt 3: " + prompt);
                }
            }
            else
            {
                Console.WriteLine("X");
            }
        }

        public string SendCommand(string cmd, ShellStream sh, int delay)
        {
            StreamReader reader = null;
            string response = "";
            try
            {
                reader = new StreamReader(sh);
                StreamWriter writer = new StreamWriter(sh);
                writer.AutoFlush = true;
                writer.WriteLine(cmd + writer.NewLine);
                
        //        while (sh.Length == 0)
                {
                    Thread.Sleep(delay);
                }
                Regex rgx = new Regex(@"\d{4} \d{1,7}LDM");
                response = reader.ReadToEnd();

                response = rgx.Replace(response,System.Environment.NewLine + "LDM");

            }
            catch (Exception ex)
            {
                Console.WriteLine("exception: " + ex.Message);
                if(debugging)
                    Console.WriteLine("Stack: " + ex.StackTrace);
            }
            if (debugging && response!="")
                Console.WriteLine(response);
            return response;
        }

        private void OpenCSV_Click(object sender, EventArgs e)
        {
            try
            {
                if (chk_csv.Checked)
                {
                    Process csv = new Process();
                    csv.StartInfo.FileName = tb_FileName.Text + ".csv";
                    csv.Start();
                }
                if (chk_xml.Checked)
                {
                    Process xml = new Process();
                    xml.StartInfo.FileName = tb_FileName.Text + ".xml";
                    xml.Start();
                }
                if (chk_sql.Checked)
                {
                    Process sql = new Process();
                    sql.StartInfo.FileName = tb_FileName.Text + "-universe.sql";
                    sql.Start();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("exception: " + ex.Message);
                if (debugging)
                    Console.WriteLine("Stack: " + ex.StackTrace);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tb_FileName.Text = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + @"\GUIDs\" + cb_ldmName.Items[cb_ldmName.SelectedIndex].ToString();

            tb_FileName.SelectionStart = tb_FileName.Text.Length;

            cb_env.Select();
        }

        private void chk_GetAll_CheckedChanged(object sender, EventArgs e)
        {
            num_sampleSize.Enabled = !chk_GetAll.Checked;
        }


        public string GEThttp(Environment environment, string resource, string guid, string token)
        {
            WebRequest request = WebRequest.Create(environment.API_URL + resource + "/" + guid);
            request.Method = "GET";
            request.ContentType = "application/JSON";
            request.Headers.Add("X-CustomCredentials", token);
            
            WebResponse responseObj = request.GetResponse();            
            Stream data = responseObj.GetResponseStream();            
            StreamReader reader = new StreamReader(data);

            string response = reader.ReadToEnd();

            responseObj.Close();
            data.Close();
            reader.Close();
            return response;
        }

        public string Authenticate(string api)
        {
            WebRequest request = WebRequest.Create(api + "session/login");
            request.Method = "POST";

            string postData = "{'UserId': 'svc_progid6','Password': 'prOgID06'}";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse responseObj = request.GetResponse();
            Stream data = responseObj.GetResponseStream();
            StreamReader reader = new StreamReader(data);

            string response = reader.ReadToEnd();

            responseObj.Close();
            data.Close();
            reader.Close();
            return response;
        }

        private void browse_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if(result == DialogResult.OK)
            {
                tb_csv_input.Text = openFileDialog1.FileName;
            }
        }

        private void btn_browseSave_Click(object sender, EventArgs e)
        {
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                tb_FileName.Text = saveFileDialog1.FileName;
                tb_FileName.SelectionStart = tb_FileName.Text.Length;
            }
        }

        private void cb_ldmName_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_FileName.Text = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + @"\GUIDs\" + cb_ldmName.Items[cb_ldmName.SelectedIndex].ToString();
            tb_FileName.SelectionStart = tb_FileName.Text.Length;
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void cb_env_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CustomQuery_Click(object sender, EventArgs e)
        {

        }

        private void tab_SearchMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tab_SearchMethod.SelectedIndex == 2)
            {
                chk_csv.Enabled = false;
                chk_xml.Enabled = false;
                chk_sql.Enabled = false;

                chk_csv.Checked = true;
                chk_xml.Checked = false;
                chk_sql.Checked = false;
            }
            else
            {
                chk_csv.Enabled = true;
                chk_xml.Enabled = true;
                chk_sql.Enabled = true;

                chk_csv.Checked = false;
                chk_xml.Checked = false;
                chk_sql.Checked = false;
            }
            
        }
    }
}
