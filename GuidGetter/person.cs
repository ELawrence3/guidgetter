﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuidGetter
{
    public class Person
    {
        public PrivacyStatus privacyStatus { get; set; }
        public List<Name> names { get; set; }
        public string dateOfBirth { get; set; }
        public string gender { get; set; }
        public Ethnicity ethnicity { get; set; }
        public List<Race> races { get; set; }
        public MaritalStatus maritalStatus { get; set; }
        public List<Role> roles { get; set; }
        public List<Credential> credentials { get; set; }
        public List<Address> addresses { get; set; }
        public List<Phone> phones { get; set; }
        public List<Email> emails { get; set; }
        public Metadata metadata { get; set; }
        public string id { get; set; }
        public Military military { get; set; }
    }
    public class PrivacyStatus
    {
        public string privacyCategory { get; set; }
    }

    public class Detail
    {
        public string id { get; set; }
    }

    public class Type
    {
        public string category { get; set; }
        public Detail detail { get; set; }

        public static implicit operator Type(System.Type v)
        {
            throw new NotImplementedException();
        }
    }

    public class Name
    {
        public Type type { get; set; }
        public string fullName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string preference { get; set; }
    }

    public class EthnicGroup
    {
        public string id { get; set; }
    }

    public class Country
    {
        public string code { get; set; }
        public string ethnicCategory { get; set; }
    }

    public class Reporting
    {
        public Country country { get; set; }
    }

    public class Ethnicity
    {
        public EthnicGroup ethnicGroup { get; set; }
        public List<Reporting> reporting { get; set; }
    }

    public class Race2
    {
        public string id { get; set; }
    }

    public class Country2
    {
        public string code { get; set; }
        public string racialCategory { get; set; }
    }

    public class Reporting2
    {
        public Country2 country { get; set; }
    }

    public class Race
    {
        public Race2 race { get; set; }
        public List<Reporting2> reporting { get; set; }
    }

    public class Detail2
    {
        public string id { get; set; }
    }

    public class MaritalStatus
    {
        public string maritalCategory { get; set; }
        public Detail2 detail { get; set; }
    }

    public class Role
    {
        public string role { get; set; }
    }

    public class Credential
    {
        public string type { get; set; }
        public string value { get; set; }
    }

    public class Address2
    {
        public string id { get; set; }
    }

    public class Detail3
    {
        public string id { get; set; }
    }

    public class Type2
    {
        public string addressType { get; set; }
        public Detail3 detail { get; set; }
    }

    public class Address
    {
        public Address2 address { get; set; }
        public Type2 type { get; set; }
        public DateTime startOn { get; set; }
        public string preference { get; set; }
    }

    public class Detail4
    {
        public string id { get; set; }
    }

    public class Type3
    {
        public string phoneType { get; set; }
        public Detail4 detail { get; set; }
    }

    public class Phone
    {
        public Type3 type { get; set; }
        public string number { get; set; }
        public string extension { get; set; }
    }

    public class Detail5
    {
        public string id { get; set; }
    }

    public class Type4
    {
        public string emailType { get; set; }
        public Detail5 detail { get; set; }
    }

    public class Email
    {
        public Type4 type { get; set; }
        public string preference { get; set; }
        public string address { get; set; }
    }

    public class Metadata
    {
    }

    public class Military
    {
        public string selectiveServiceFlag { get; set; }
    }
}
