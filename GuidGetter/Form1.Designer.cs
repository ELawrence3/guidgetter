﻿namespace GuidGetter
{
    partial class GUIGUIDGETTER
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GET = new System.Windows.Forms.Button();
            this.tb_FileName = new System.Windows.Forms.TextBox();
            this.FileNameLabel = new System.Windows.Forms.Label();
            this.output = new System.Windows.Forms.TextBox();
            this.cb_ldmName = new System.Windows.Forms.ComboBox();
            this.lb_resource = new System.Windows.Forms.Label();
            this.OpenCSV = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.chk_GetAll = new System.Windows.Forms.CheckBox();
            this.num_sampleSize = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cb_env = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.tb_username = new System.Windows.Forms.TextBox();
            this.tab_SearchMethod = new System.Windows.Forms.TabControl();
            this.byResource = new System.Windows.Forms.TabPage();
            this.ByCID = new System.Windows.Forms.TabPage();
            this.cb_resourceByID = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_csv_input = new System.Windows.Forms.TextBox();
            this.browse = new System.Windows.Forms.Button();
            this.tb_CID = new System.Windows.Forms.TextBox();
            this.CustomQuery = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.chk_csv = new System.Windows.Forms.CheckBox();
            this.chk_xml = new System.Windows.Forms.CheckBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btn_browseSave = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.chk_append = new System.Windows.Forms.CheckBox();
            this.chk_sql = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.num_sampleSize)).BeginInit();
            this.panel1.SuspendLayout();
            this.tab_SearchMethod.SuspendLayout();
            this.byResource.SuspendLayout();
            this.ByCID.SuspendLayout();
            this.SuspendLayout();
            // 
            // GET
            // 
            this.GET.Location = new System.Drawing.Point(368, 257);
            this.GET.Name = "GET";
            this.GET.Size = new System.Drawing.Size(75, 23);
            this.GET.TabIndex = 12;
            this.GET.Text = "Get GUIDs";
            this.GET.UseVisualStyleBackColor = true;
            this.GET.Click += new System.EventHandler(this.GET_Click);
            // 
            // tb_FileName
            // 
            this.tb_FileName.Location = new System.Drawing.Point(109, 232);
            this.tb_FileName.Name = "tb_FileName";
            this.tb_FileName.Size = new System.Drawing.Size(334, 20);
            this.tb_FileName.TabIndex = 7;
            this.tb_FileName.Text = "output";
            // 
            // FileNameLabel
            // 
            this.FileNameLabel.AutoSize = true;
            this.FileNameLabel.Location = new System.Drawing.Point(17, 236);
            this.FileNameLabel.Name = "FileNameLabel";
            this.FileNameLabel.Size = new System.Drawing.Size(92, 13);
            this.FileNameLabel.TabIndex = 2;
            this.FileNameLabel.Text = "Output File Name:";
            // 
            // output
            // 
            this.output.Location = new System.Drawing.Point(285, 11);
            this.output.Multiline = true;
            this.output.Name = "output";
            this.output.ReadOnly = true;
            this.output.Size = new System.Drawing.Size(249, 100);
            this.output.TabIndex = 99;
            this.output.TabStop = false;
            // 
            // cb_ldmName
            // 
            this.cb_ldmName.FormattingEnabled = true;
            this.cb_ldmName.Items.AddRange(new object[] {
            "academic-catalogs",
            "academic-credentials",
            "academic-credit",
            "academic-disciplines",
            "academic-honors",
            "academic-levels",
            "academic-periods",
            "academic-programs",
            "academic-standings",
            "accounting-code-categories",
            "accounting-codes",
            "accounting-string-component-values",
            "accounting-string-subcomponents",
            "accounting-string-subcomponent-values",
            "account-receivable-types",
            "accounts-payable-invoices",
            "accounts-payable-sources",
            "accounts-receivable-account-details",
            "accounts-receivable-term-summaries",
            "addresses",
            "admission-applications",
            "admission-application-status-types",
            "admission-application-supporting-items",
            "admission-application-supporting-item-types",
            "admission-application-types",
            "admission-application-withdrawal-reasons",
            "admission-decisions",
            "admission-decision-types",
            "admission-populations",
            "admission-residency-types",
            "aptitude-assessments",
            "bargaining-units",
            "blanket-purchase-orders",
            "budget-codes",
            "budget-phase-line-items",
            "budget-phases",
            "buildings",
            "buyers",
            "campus-involvement-roles",
            "campus-involvements",
            "campus-organizations",
            "campus-organization-types",
            "comments",
            "commerce-tax-codes",
            "commodity-codes",
            "commodity-unit-types",
            "contribution-payroll-deductions",
            "courses",
            "credit-categories",
            "deduction-types",
            "earning-types",
            "educational-institutions",
            "educational-institution-units",
            "employee-leave-plans",
            "employee-leave-transactions",
            "employees",
            "employment-departments",
            "employment-performance-reviews",
            "employment-proficiencies",
            "employment-vocations",
            "external-education",
            "external-employments",
            "financial-aid-academic-progress-types",
            "financial-aid-application-outcomes",
            "financial-aid-applications",
            "financial-aid-award-periods",
            "financial-aid-fund-categories",
            "financial-aid-fund-classifications",
            "financial-aid-funds",
            "financial-aid-offices",
            "financial-aid-years",
            "fiscal-periods",
            "fiscal-years",
            "fixed-asset-categories",
            "fixed-assets",
            "fixed-asset-types",
            "free-on-board-types",
            "general-ledger-transactions",
            "geographic-areas",
            "grade-definitions",
            "grade-schemes",
            "grants",
            "housing-assignments",
            "housing-requests",
            "institution-jobs",
            "institution-positions",
            "instructional-events",
            "instructional-methods",
            "instructors",
            "interests",
            "job-applications",
            "leave-plans",
            "ledger-activities",
            "meal-plan-assignments",
            "meal-plan-rates",
            "meal-plan-requests",
            "meal-plans",
            "organizations",
            "pay-classes",
            "pay-classifications",
            "pay-cycles",
            "payment-transactions",
            "pay-periods",
            "payroll-deduction-arrangements",
            "pay-scales",
            "personal-relationships",
            "personal-relationship-types",
            "person-contacts",
            "person-employment-proficiencies",
            "person-filters",
            "person-holds",
            "person-hold-types",
            "persons",
            "person-visas",
            "procurement-receipts",
            "purchase-orders",
            "registration-drop-reason",
            "religions",
            "requisitions",
            "resident-types",
            "restricted-student-financial-aid-awards",
            "room-rates",
            "rooms",
            "room-types",
            "section-crosslists",
            "section-instructors",
            "sections",
            "shipping-methods",
            "ship-to-destinations",
            "sites",
            "student-academic-period-profiles",
            "student-academic-programs",
            "student-academic-standings",
            "student-advisor-relationships",
            "student-aptitude-assessments",
            "student-charges",
            "student-classifications",
            "student-course-transfers",
            "student-financial-aid-academic-progress-statuses",
            "student-financial-aid-awards",
            "student-financial-aid-award-supporting-items",
            "student-financial-aid-decisions",
            "student-financial-aid-need-summaries",
            "student-overall-gpa",
            "student-payments",
            "student-pending-financial-aid-details",
            "student-program-remaining-credits",
            "students",
            "student-section-waitlists",
            "student-types",
            "subjects",
            "topics",
            "vendor-classifications",
            "vendor-payment-terms",
            "vendors",
            "person-hold-supporting-items",
            "CIMC",
            "EAEE",
            "EDTC"});
            this.cb_ldmName.Location = new System.Drawing.Point(70, 20);
            this.cb_ldmName.Name = "cb_ldmName";
            this.cb_ldmName.Size = new System.Drawing.Size(208, 21);
            this.cb_ldmName.TabIndex = 4;
            this.cb_ldmName.Text = "persons";
            this.cb_ldmName.SelectedIndexChanged += new System.EventHandler(this.cb_ldmName_SelectedIndexChanged);
            // 
            // lb_resource
            // 
            this.lb_resource.AutoSize = true;
            this.lb_resource.Location = new System.Drawing.Point(14, 24);
            this.lb_resource.Name = "lb_resource";
            this.lb_resource.Size = new System.Drawing.Size(56, 13);
            this.lb_resource.TabIndex = 6;
            this.lb_resource.Text = "Resource:";
            // 
            // OpenCSV
            // 
            this.OpenCSV.Location = new System.Drawing.Point(455, 257);
            this.OpenCSV.Name = "OpenCSV";
            this.OpenCSV.Size = new System.Drawing.Size(75, 23);
            this.OpenCSV.TabIndex = 13;
            this.OpenCSV.Text = "Open";
            this.OpenCSV.UseVisualStyleBackColor = true;
            this.OpenCSV.Click += new System.EventHandler(this.OpenCSV_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(300, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Sample Size:";
            // 
            // chk_GetAll
            // 
            this.chk_GetAll.AutoSize = true;
            this.chk_GetAll.Location = new System.Drawing.Point(423, 22);
            this.chk_GetAll.Name = "chk_GetAll";
            this.chk_GetAll.Size = new System.Drawing.Size(37, 17);
            this.chk_GetAll.TabIndex = 6;
            this.chk_GetAll.Text = "All";
            this.chk_GetAll.UseVisualStyleBackColor = true;
            this.chk_GetAll.CheckedChanged += new System.EventHandler(this.chk_GetAll_CheckedChanged);
            // 
            // num_sampleSize
            // 
            this.num_sampleSize.Location = new System.Drawing.Point(374, 20);
            this.num_sampleSize.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.num_sampleSize.Name = "num_sampleSize";
            this.num_sampleSize.Size = new System.Drawing.Size(43, 20);
            this.num_sampleSize.TabIndex = 5;
            this.num_sampleSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cb_env);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tb_password);
            this.panel1.Controls.Add(this.tb_username);
            this.panel1.Location = new System.Drawing.Point(12, 11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(249, 100);
            this.panel1.TabIndex = 13;
            // 
            // cb_env
            // 
            this.cb_env.FormattingEnabled = true;
            this.cb_env.Items.AddRange(new object[] {
            "dev",
            "stage",
            "test1",
            "test2",
            "test3",
            "test4"});
            this.cb_env.Location = new System.Drawing.Point(117, 8);
            this.cb_env.Name = "cb_env";
            this.cb_env.Size = new System.Drawing.Size(100, 21);
            this.cb_env.TabIndex = 0;
            this.cb_env.Text = "test4";
            this.cb_env.SelectedIndexChanged += new System.EventHandler(this.cb_env_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(14, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 17;
            this.label3.Text = "Environment:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 16;
            this.label2.Text = "Username:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 15;
            this.label1.Text = "Password:";
            // 
            // tb_password
            // 
            this.tb_password.Location = new System.Drawing.Point(117, 66);
            this.tb_password.Name = "tb_password";
            this.tb_password.PasswordChar = '*';
            this.tb_password.Size = new System.Drawing.Size(100, 20);
            this.tb_password.TabIndex = 2;
            // 
            // tb_username
            // 
            this.tb_username.Location = new System.Drawing.Point(117, 37);
            this.tb_username.Name = "tb_username";
            this.tb_username.Size = new System.Drawing.Size(100, 20);
            this.tb_username.TabIndex = 1;
            // 
            // tab_SearchMethod
            // 
            this.tab_SearchMethod.Controls.Add(this.byResource);
            this.tab_SearchMethod.Controls.Add(this.ByCID);
            this.tab_SearchMethod.Controls.Add(this.CustomQuery);
            this.tab_SearchMethod.Location = new System.Drawing.Point(12, 134);
            this.tab_SearchMethod.Name = "tab_SearchMethod";
            this.tab_SearchMethod.SelectedIndex = 0;
            this.tab_SearchMethod.Size = new System.Drawing.Size(522, 96);
            this.tab_SearchMethod.TabIndex = 3;
            // 
            // byResource
            // 
            this.byResource.Controls.Add(this.cb_ldmName);
            this.byResource.Controls.Add(this.lb_resource);
            this.byResource.Controls.Add(this.num_sampleSize);
            this.byResource.Controls.Add(this.label4);
            this.byResource.Controls.Add(this.chk_GetAll);
            this.byResource.Location = new System.Drawing.Point(4, 22);
            this.byResource.Name = "byResource";
            this.byResource.Padding = new System.Windows.Forms.Padding(3);
            this.byResource.Size = new System.Drawing.Size(514, 70);
            this.byResource.TabIndex = 0;
            this.byResource.Text = "By Resource";
            this.byResource.UseVisualStyleBackColor = true;
            // 
            // ByCID
            // 
            this.ByCID.Controls.Add(this.cb_resourceByID);
            this.ByCID.Controls.Add(this.label6);
            this.ByCID.Controls.Add(this.label9);
            this.ByCID.Controls.Add(this.label8);
            this.ByCID.Controls.Add(this.label7);
            this.ByCID.Controls.Add(this.tb_csv_input);
            this.ByCID.Controls.Add(this.browse);
            this.ByCID.Controls.Add(this.tb_CID);
            this.ByCID.Location = new System.Drawing.Point(4, 22);
            this.ByCID.Name = "ByCID";
            this.ByCID.Padding = new System.Windows.Forms.Padding(3);
            this.ByCID.Size = new System.Drawing.Size(514, 70);
            this.ByCID.TabIndex = 1;
            this.ByCID.Text = "By Colleague ID";
            this.ByCID.UseVisualStyleBackColor = true;
            // 
            // cb_resourceByID
            // 
            this.cb_resourceByID.FormattingEnabled = true;
            this.cb_resourceByID.Items.AddRange(new object[] {
            "persons"});
            this.cb_resourceByID.Location = new System.Drawing.Point(79, 6);
            this.cb_resourceByID.Name = "cb_resourceByID";
            this.cb_resourceByID.Size = new System.Drawing.Size(208, 21);
            this.cb_resourceByID.TabIndex = 7;
            this.cb_resourceByID.Text = "persons";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Resource:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Colleague ID:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(254, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Spreadsheet:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(206, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "OR";
            // 
            // tb_csv_input
            // 
            this.tb_csv_input.Location = new System.Drawing.Point(327, 38);
            this.tb_csv_input.Name = "tb_csv_input";
            this.tb_csv_input.Size = new System.Drawing.Size(100, 20);
            this.tb_csv_input.TabIndex = 5;
            // 
            // browse
            // 
            this.browse.Location = new System.Drawing.Point(433, 37);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(75, 23);
            this.browse.TabIndex = 6;
            this.browse.Text = "Browse...";
            this.browse.UseVisualStyleBackColor = true;
            this.browse.Click += new System.EventHandler(this.browse_Click);
            // 
            // tb_CID
            // 
            this.tb_CID.Location = new System.Drawing.Point(79, 38);
            this.tb_CID.Name = "tb_CID";
            this.tb_CID.Size = new System.Drawing.Size(100, 20);
            this.tb_CID.TabIndex = 4;
            // 
            // CustomQuery
            // 
            this.CustomQuery.Location = new System.Drawing.Point(4, 22);
            this.CustomQuery.Name = "CustomQuery";
            this.CustomQuery.Padding = new System.Windows.Forms.Padding(3);
            this.CustomQuery.Size = new System.Drawing.Size(514, 70);
            this.CustomQuery.TabIndex = 2;
            this.CustomQuery.Text = "Custom Query";
            this.CustomQuery.UseVisualStyleBackColor = true;
            this.CustomQuery.Click += new System.EventHandler(this.CustomQuery_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Search Method:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "csv";
            this.openFileDialog1.FileName = "csv spreadhseet";
            this.openFileDialog1.Filter = "CSV files|*.csv";
            // 
            // chk_csv
            // 
            this.chk_csv.AutoSize = true;
            this.chk_csv.Location = new System.Drawing.Point(112, 260);
            this.chk_csv.Name = "chk_csv";
            this.chk_csv.Size = new System.Drawing.Size(46, 17);
            this.chk_csv.TabIndex = 9;
            this.chk_csv.Text = ".csv";
            this.chk_csv.UseVisualStyleBackColor = true;
            // 
            // chk_xml
            // 
            this.chk_xml.AutoSize = true;
            this.chk_xml.Location = new System.Drawing.Point(158, 260);
            this.chk_xml.Name = "chk_xml";
            this.chk_xml.Size = new System.Drawing.Size(44, 17);
            this.chk_xml.TabIndex = 10;
            this.chk_xml.Text = ".xml";
            this.chk_xml.UseVisualStyleBackColor = true;
            // 
            // btn_browseSave
            // 
            this.btn_browseSave.Location = new System.Drawing.Point(455, 229);
            this.btn_browseSave.Name = "btn_browseSave";
            this.btn_browseSave.Size = new System.Drawing.Size(75, 23);
            this.btn_browseSave.TabIndex = 8;
            this.btn_browseSave.Text = "Browse...";
            this.btn_browseSave.UseVisualStyleBackColor = true;
            this.btn_browseSave.Click += new System.EventHandler(this.btn_browseSave_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 262);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Output File Types:";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // chk_append
            // 
            this.chk_append.AutoSize = true;
            this.chk_append.Location = new System.Drawing.Point(250, 260);
            this.chk_append.Name = "chk_append";
            this.chk_append.Size = new System.Drawing.Size(110, 17);
            this.chk_append.TabIndex = 11;
            this.chk_append.Text = "append (csv only)";
            this.chk_append.UseVisualStyleBackColor = true;
            // 
            // chk_sql
            // 
            this.chk_sql.AutoSize = true;
            this.chk_sql.Location = new System.Drawing.Point(202, 260);
            this.chk_sql.Name = "chk_sql";
            this.chk_sql.Size = new System.Drawing.Size(42, 17);
            this.chk_sql.TabIndex = 100;
            this.chk_sql.Text = ".sql";
            this.chk_sql.UseVisualStyleBackColor = true;
            // 
            // GUIGUIDGETTER
            // 
            this.AcceptButton = this.GET;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 286);
            this.Controls.Add(this.chk_sql);
            this.Controls.Add(this.chk_append);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btn_browseSave);
            this.Controls.Add(this.chk_xml);
            this.Controls.Add(this.chk_csv);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tab_SearchMethod);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.OpenCSV);
            this.Controls.Add(this.output);
            this.Controls.Add(this.FileNameLabel);
            this.Controls.Add(this.tb_FileName);
            this.Controls.Add(this.GET);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "GUIGUIDGETTER";
            this.Text = "GUID Getter GUI";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.num_sampleSize)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tab_SearchMethod.ResumeLayout(false);
            this.byResource.ResumeLayout(false);
            this.byResource.PerformLayout();
            this.ByCID.ResumeLayout(false);
            this.ByCID.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GET;
        private System.Windows.Forms.TextBox tb_FileName;
        private System.Windows.Forms.Label FileNameLabel;
        private System.Windows.Forms.TextBox output;
        private System.Windows.Forms.ComboBox cb_ldmName;
        private System.Windows.Forms.Label lb_resource;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.TextBox tb_username;
        private System.Windows.Forms.ComboBox cb_env;
        private System.Windows.Forms.Button OpenCSV;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chk_GetAll;
        private System.Windows.Forms.NumericUpDown num_sampleSize;
        private System.Windows.Forms.TabControl tab_SearchMethod;
        private System.Windows.Forms.TabPage byResource;
        private System.Windows.Forms.TabPage ByCID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_csv_input;
        private System.Windows.Forms.Button browse;
        private System.Windows.Forms.TextBox tb_CID;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.CheckBox chk_csv;
        private System.Windows.Forms.CheckBox chk_xml;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btn_browseSave;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cb_resourceByID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chk_append;
        private System.Windows.Forms.CheckBox chk_sql;
        private System.Windows.Forms.TabPage CustomQuery;
    }
}

